import React from "react";
import ReactDOM from "react-dom";
import Clock from './Clock';
//import SeasonDetail from "./SeasonDetail"
if (module.hot) {
  module.hot.accept();
}

class App extends React.Component {
 state = { time : new Date().toLocaleTimeString() }
  
  componentDidMount() {
    setInterval(() => {
      this.setState({ time: new Date().toLocaleTimeString() });
    }, 1000);
  }



    render(){

  return <div className = "ui segment middle aligned" style = {{margin : '50px',textAlign : 'center'}}>
     <Clock time = {this.state.time}/>
  </div>
    }
};

ReactDOM.render(<App />, document.querySelector("#root"));
