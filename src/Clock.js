import React from 'react';

const Clock = (props) => {
    console.log(props);
    return (
        <div class="ui statistic">
  <div class="label" style = {{color: "#FBBD08"}}>
    Current Time : 
  </div>
    
        <div class="value " style={{color : '#00B5AD'}}>
            {props.time}
      
  </div>
</div>
    )
    
};

export default Clock;